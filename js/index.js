function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout-btn").addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert("Logout Success!");
                    window.location = "index.html";
                }).catch(function(){
                    alert("Logout Error!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field\
            var db = firebase.database();
            var comRef = db.ref('com_list');
            var newComment = comRef.push();
            newComment.set({
                "email" : document.getElementById("dynamic-menu").children[0].innerHTML,
                "post": post_txt.value
            }).then(function(){
                post_txt.value = "";
            });
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var obj = snapshot.val();
            posts = Object.values(obj);

            for(i = 0; i < posts.length; i++)
            {
                var s = str_before_username + posts[i].email + "</strong>" + posts[i].post + str_after_content;
                total_post.push(s);
            }
            // set first count
            first_count = posts.length;

            document.getElementById("post_list").innerHTML = total_post.join("\n");
            total_post = [];

            postsRef.on('value', function(snapshot){
                var obj = snapshot.val();
                posts = Object.values(obj);
                if(posts.length <= first_count) return;

                // add only new posts
                total_post = [];
                for(i = first_count; i < posts.length; i++)
                {
                    var s = str_before_username + posts[i].email + "</strong>" + posts[i].post + str_after_content;
                    total_post.push(s);
                }
                first_count = posts.length;
                document.getElementById("post_list").innerHTML += total_post.join("\n");


            });

            
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};